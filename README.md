# lderamo

This package is a modified version of koza4ok package that can be found [here](https://github.com/yuraic/koza4ok) adapted for the needs of the VHbb analysis and the package of A. Rogozhnikov [hep-ml](https://github.com/arogozhnikov/hep_ml).

The package contains scikit-learn to TMVA convertor called ```skTMVA```. The idea is to save scikit-learn BDT model to the TMVA xml-file. This allows you to use scikit-learn model directly from TMVA. Once the model is trained and converted, scikit-learn library is not needed anymore! The classification task can be performed with TMVA/ROOT only. This is particularly useful within ATLAS framework where there is no scikit-learn installed. A user can train the classifier with scikit-learn on his laptop and later use in ATLAS framework converted to the TMVA xml-file. 

## Dependencies
- [ROOT](http://root.cern.ch) (with TMVA package)
- [NumPy](http://www.numpy.org/)
- [scikit-learn](http://scikit-learn.org/)
- [hep-ml](https://github.com/arogozhnikov/hep_ml)
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

## Installation
Basically just add package root directory to your PYTHONPATH enviroment variable. Or you can do this,
```
> source setup.sh
```

## skTMVA converter

To convert BDT to TMVA xml-file, use the following method in your <b>python</b> code (see exemples),
```python
convert_bdt_sklearn_tmva(bdt, [('var1', 'F'), ('var2', 'F')]], columns_min, columns_max, 'bdt_sklearn_to_tmva_example.xml')
```
where 

- ```bdt ``` is your scikit-learn trained model, 
- ```'[('var1', 'F'), ('var2', 'F')]' ``` is the input variable description for TMVA. It consists of variable names and their basic types (e.g. ```'F'``` is for float). Please note, that the ordering here must be the same as the order of columns in your numpy array,
- ``` columns_min, columns_max ```  are numpy arrays containing min/max values for the variables listed before (same order). 
- ```bdt_sklearn_to_tmva_example.xml ``` is the output TMVA xml-file


> Supports: AdaBoost, Gradient and UGradient (from hep-ml) Boosting decision trees for binary classification. 

> In terms of High-Energy Physics jargon, AdaBoost or Gradient Boosting BDTs for signal and background discrimination. 

## Contacts

For any question, suggestion or comment, please don't hesitate to contact me @ lderamo@cern.ch


